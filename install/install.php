<?php
/**
 * @package n3t Fields
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2017-2024 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

// no direct access
defined( '_JEXEC' ) or die;

use Joomla\CMS\Factory;
use Joomla\CMS\Installer\Adapter\PluginAdapter;
use Joomla\CMS\Installer\InstallerScript;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Version;

class plgSystemN3tFieldsInstallerScript extends InstallerScript
{
  const LANG_PREFIX = 'PLG_SYSTEM_N3TFIELDS';
	protected $minimumPhp = '7.4.0';
	protected $minimumJoomla = '3.10.0';
  protected $deleteFolders = [
    '/plugins/system/n3tfields/fields',
    '/plugins/system/n3tfields/layouts',
    '/plugins/system/n3tfields/helpers/convertor',
  ];
  protected $deleteFiles = [
    '/plugins/system/n3tfields/helpers/convertor.php',
  ];
  private $oldRelease = null;

	/**
	 * @inheritdoc
	 *
	 * @since 4.0.0
	 */
	public function preflight($type, $parent)
	{
		$return = parent::preflight($type, $parent);

		if (strtolower($type) === 'update') {
      if (Version::MAJOR_VERSION === 3) {
        $db = Factory::getDbo();
        $manifest = $this->getItemArray('manifest_cache', '#__extensions', 'name', $db->quote($this->extension));
      } else {
        $manifest = $this->getItemArray('manifest_cache', '#__extensions', 'name', $this->extension);
      }

			if (isset($manifest['version'])) {
				$this->oldRelease = $manifest['version'];
      }
		}

		return $return;
	}

  /**
   * runs after installation
   *
   * @since 4.1.0
   */
  public function postflight($type, $parent)
  {
    if (strtolower($type) === 'update') {
      $this->removeFiles();
    }
  }

	/**
	 * @inheritdoc
	 *
	 * @since 4.0.0
	 */
	public function update(PluginAdapter $parent)
	{
		if ($this->oldRelease) {
			Factory::getApplication()->enqueueMessage(Text::sprintf(self::LANG_PREFIX . '_INSTALL_UPDATE_VERSION', $this->oldRelease, $this->release));
		}
	}

}
