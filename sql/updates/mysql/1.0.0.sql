CREATE TABLE IF NOT EXISTS `#__n3tfields` (
  `context` varchar(255) NOT NULL DEFAULT "",
  `item_id` int(11) NOT NULL DEFAULT 0,
  `value` longtext null,
  PRIMARY KEY (`context`, `item_id`)
) DEFAULT CHARSET=utf8;

ALTER TABLE `#__n3tfields` ADD COLUMN `context` varchar(255) NOT NULL DEFAULT "";
ALTER TABLE `#__n3tfields` ADD COLUMN `item_id` int(11) NOT NULL DEFAULT 0;
ALTER TABLE `#__n3tfields` ADD COLUMN `value` longtext null;
ALTER TABLE `#__n3tfields` ADD PRIMARY KEY (`context`, `item_id`);