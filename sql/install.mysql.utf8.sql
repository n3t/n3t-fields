/**
 * @package n3t Workflow
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2017-2024 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

CREATE TABLE IF NOT EXISTS `#__n3tfields` (
  `context` varchar(255) NOT NULL DEFAULT "",
  `item_id` int(11) NOT NULL DEFAULT 0,
  `value` longtext null,
  PRIMARY KEY (`context`, `item_id`)
) DEFAULT CHARSET=utf8;
