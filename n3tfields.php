<?php
/**
 * @package n3t Fields
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2017-2024 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

use Joomla\CMS\Component\ComponentHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\Filesystem\File;
use Joomla\CMS\Form\Form;
use Joomla\CMS\Form\FormHelper;
use Joomla\CMS\Plugin\CMSPlugin;
use Joomla\CMS\Version;
use Joomla\Registry\Registry;

class plgSystemN3tFields extends CMSPlugin {

  /**
   * Affects constructor behavior. If true, language files will be loaded automatically.
   *
   * @var    boolean
   * @since  4.0.0
   */
  protected $autoloadLanguage = true;

  /**
   * Affects constructor behavior. If true, language files will be loaded automatically.
   *
   * @var    Joomla\CMS\Application\CMSApplication
   * @since  4.0.0
   */
  protected $app = null;

  /**
   * Debug data
   *
   * @var	   array
   * @since  4.0.0
   */
  private $debug = [];

  public function __construct(&$subject, $config = [])
  {
    parent::__construct($subject, $config);

    $this->app = Factory::getApplication();

    if (Version::MAJOR_VERSION < 4)
      JLoader::registerNamespace('Joomla\\Plugin\\System\\n3tFields', __DIR__ . '/src/n3tFields', false, false, 'psr4');
    else
      JLoader::registerNamespace('Joomla\\Plugin\\System\\n3tFields', __DIR__ . '/src/n3tFields');

    if (class_exists('\\n3tDebug') && method_exists('\\n3tDebug', 'registerPanel'))
      n3tDebug::registerPanel(new \Joomla\Plugin\System\n3tFields\Debug\Panel($this));
  }

  protected function getTemplates()
  {
    static $templates = null;

    if ($templates === null) {
      $db = Factory::getDbo();
      $query = $db->getQuery(true)
        ->select($db->quoteName('a.name'))
        ->from($db->quoteName('#__extensions', 'a'))
        ->where($db->quoteName('a.client_id') . ' = 0')
        ->where($db->quoteName('a.enabled') . ' = 1')
        ->where($db->quoteName('a.type') . ' = ' . $db->quote('template'))
        ->order($db->quoteName('a.name') . ' ASC');
      $db->setQuery($query);
      $templates = $db->loadColumn();
    }

    return $templates;
  }

  protected function getFiles($name)
  {
    $files = array();
    $file = str_replace('.', '/', $name) . '.xml';

    foreach ($this->getTemplates() as $template) {
      if (File::exists(JPATH_SITE . '/templates/' . $template . '/n3tfields/' . $file))
        $files[] = [
          'path' => JPATH_SITE . '/templates/' . $template . '/n3tfields/' . $file,
          'template' => $template,
        ];
    }

    return $files;
  }

  protected function getContext($context)
  {
    // com_categories
    if (strpos($context, 'com_categories.category') === 0)
      $context = $this->app->input->getCmd('extension') . '.category';
    // Users profile aliases
    else if (in_array($context, array('com_users.profile', 'com_users.user', 'com_admin.profile')))
      $context = 'com_users.user';
    // Frontend module manager
    else if ($this->app->isClient('site') && $context == 'com_config.module')
      $context = 'com_modules.module';
    // Frontend contact form
    else if ($this->app->isClient('site') && $context == 'com_contact.contact')
      $context = 'com_contact.contactform';
    // Regular Labs Advanced module manager
    else if ($context == 'com_advancedmodules.module')
      $context = 'com_modules.module';
    // com_fields
    else if (strpos($context, 'com_fields.field') === 0)
      $context = 'com_fields.field';

    return $context;
  }

  public function onAfterInitialise()
  {
    JLoader::register('n3tfields', __DIR__ . '/helpers/n3tfields.php');
    n3tFields::registerContext('article', 'com_content.article');
    n3tFields::registerContext('banner', 'com_banners.banner');
    n3tFields::registerContext('category', 'com_content.category');
    n3tFields::registerContext('contact', 'com_contact.contact');
    n3tFields::registerContext('menu', 'com_menus.item');
    n3tFields::registerContext('module', 'com_modules.module');
    n3tFields::registerContext('tag', 'com_tags.tag');
    n3tFields::registerContext('user', 'com_users.user');
    n3tFields::registerContext('field', 'com_fields.field');
  }

  public function onContentPrepareForm($form, $data)
  {
    if (!($form instanceof Form)) {
      $this->_subject->setError('JERROR_NOT_A_FORM');
      return false;
    }

    $data = (array)$data;
    $context = $this->getContext($form->getName());
    $this->debug[] = $context;

    $files = $this->getFiles($context);

    // Context extensions
    switch ($context) {
      case 'com_modules.module':
        if (isset($data['module'])) {
          $files = array_merge($files, $this->getFiles($context . '.' . $data['module']));
          $this->debug[] = $context . '.' . $data['module'];
        }
        break;
      case 'com_menus.item':
        if (isset($data['component_id'])) {
          $components = ComponentHelper::getComponents();
          foreach($components as $component) {
            if ($component->id == $data['component_id']) {
              $files = array_merge($files, $this->getFiles($context . '.' . $component->option));
              $this->debug[] = $context . '.' . $component->option;
              break;
            }
          }
        }
        break;
      case 'com_fields.field':
        $files = array_merge($files, $this->getFiles($form->getName()));
        $this->debug[] = $form->getName();
        break;
    }

    $lang = Factory::getLanguage();

    JLoader::register('FieldsHelper', JPATH_ADMINISTRATOR . '/components/com_fields/helpers/fields.php');
    $fieldTypes = FieldsHelper::getFieldTypes();
    foreach ($fieldTypes as $field) {
      if ($path = $field['path'])
        FormHelper::addFieldPath($path);

      if ($path = $field['rules'])
        FormHelper::addRulePath($path);
    }

    foreach ($files as $file) {
      $lang->load('tpl_' . $file['template'] , JPATH_SITE . '/templates/' . $file['template'], $lang->getTag(), true);
      $form->loadFile($file['path'], false);
    }

    return true;
  }

  public function onContentPrepareData($context, $data)
  {
    if (is_array($data) && isset($data['id']))
      $id = $data['id'];
    elseif (is_object($data) && isset($data->id))
      $id = $data->id;
    else
      return true;

    if (empty($id))
      return true;

    $context = $this->getContext($context);

    $db = Factory::getDbo();
    $query = $db->getQuery(true)
      ->select($db->quoteName('value'))
      ->from('#__n3tfields')
      ->where($db->quoteName('context') . ' = ' . $db->quote($context))
      ->where($db->quoteName('item_id') . ' = ' . $id);
    $db->setQuery($query);

    if (is_array($data)) {
      $data['n3tfields'] = new Registry();
      $data['n3tfields'] = $data['n3tfields']->loadString($db->loadResult() ?? '');
    } else {
      $data->n3tfields = new Registry();
      $data->n3tfields = $data->n3tfields->loadString($db->loadResult() ?? '');
    }

    return true;
  }

  public function onContentAfterSave($context, $item, $isNew, $data = [])
  {
    if (empty($data))
      $data = $this->app->input->post->get('jform', [], 'array');

    if (!is_array($data) || empty($item->id))
      return true;

    if (!isset($data['n3tfields']) || !is_array($data['n3tfields']))
      return true;

    $context = $this->getContext($context);

    $value = new stdClass();
    $value->context = $context;
    $value->item_id = $item->id;
    $value->value = new Registry();
    $value->value->loadArray($data['n3tfields'] ?? []);
    $value->value = $value->value->toString();

    $db = Factory::getDbo();
    $query = $db->getQuery(true)
      ->select($db->quoteName('item_id'))
      ->from('#__n3tfields')
      ->where($db->quoteName('context') . ' = ' . $db->quote($context))
      ->where($db->quoteName('item_id') . ' = ' . $item->id);
    $db->setQuery($query);

    if ($db->loadResult())
      $db->updateObject('#__n3tfields', $value, array('context', 'item_id'));
    else
      $db->insertObject('#__n3tfields', $value);

    n3tFields::cleanCache($context, $item);

    return true;
  }

  public function onContentAfterDelete($context, $item)
  {
    if (empty($item->id))
      return true;

    $context = $this->getContext($context);

    $db = Factory::getDbo();
    $query = $db->getQuery(true)
      ->delete('#__n3tfields')
      ->where($db->quoteName('context') . ' = ' . $db->quote($context))
      ->where($db->quoteName('item_id') . ' = ' . $item->id);
    $db->setQuery($query);
    $db->execute();

    n3tFields::cleanCache($context, $item);

    return true;
  }

  public function onExtensionAfterSave($context, $item, $isNew)
  {
    return $this->onContentAfterSave($context, $item, $isNew);
  }

  public function onExtensionAfterDelete ($context, $item)
  {
    return $this->onContentAfterDelete($context, $item);
  }

  public function onUserAfterSave($user, $isNew, $success, $msg)
  {
    if (!$user['id'] || !$success)
      return true;

    return $this->onContentAfterSave('com_users.user', Factory::getUser($user['id']), $isNew);
  }

  public function onUserAfterDelete($user, $succes, $msg)
  {
    return $this->onContentAfterDelete('com_users.user', (object)$user);
  }

  /**
   * n3tDebug collect debug data
   *
   * @since 4.0.4
   */
  public function collectDebugData()
  {
    return $this->debug;
  }

  /**
   * n3tDebug onN3tDebugAddPanel Event
   *
   * @throws Exception
   * @since 4.0.4
   */
  public function onN3tDebugAddPanel()
  {
    return new \Joomla\Plugin\System\n3tFields\Debug\Panel($this);
  }
}
