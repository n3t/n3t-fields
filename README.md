n3t Fields
==========

![Version](https://img.shields.io/badge/Latest%20version-5.0.0-informational)
![Release date](https://img.shields.io/badge/Release%20date-2024--02--08-informational)

[![Joomla! CMS](https://img.shields.io/badge/Joomla!%20CMS-V3.10-green)][JOOMLA]
[![Joomla! CMS](https://img.shields.io/badge/Joomla!%20CMS-V4.x-green)][JOOMLA]
[![PHP](https://img.shields.io/badge/PHP-V7.2-green)][PHP]
[![Documentation Status](https://readthedocs.org/projects/n3t-fields/badge/?version=latest)][DOCS]

n3t Fields adds ability to define extra fields for various Joomla administration
parts directly in template.

[JOOMLA]: https://www.joomla.org
[PHP]: https://www.php.net
[DOCS]: http://n3tfields.docs.n3t.cz/
