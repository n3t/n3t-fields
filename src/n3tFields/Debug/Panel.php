<?php
/**
 * @package n3t Fields
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2017-2024 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 **/

namespace Joomla\Plugin\System\n3tFields\Debug;

\defined('_JEXEC') or die;

class Panel extends \n3tDebug\Panel
{

	/**
	 * @var \plgSystemN3tFields
	 * @since 4.0.0
	 */
	private $plugin = null;

	public function __construct(\plgSystemN3tFields $plugin)
	{
		$this->plugin = $plugin;
		parent::__construct($plugin->params);
	}

	public function collectData(): void
  {
	  $this->data = $this->plugin->collectDebugData();
  }

  protected function getIcon(): string
  {
    $html = '';

    if ($this->data) {
      $html = '<svg viewBox="0 0 512 512" width="32" height="32">' .
        '<path fill="#666" d="m153.49 494.92c9.462 0 17.347-7.885 17.347-17.347 0-9.462-7.885-16.821-17.347-16.821h-50.99v-410.02h50.99c9.462 0 17.347-7.3593 17.347-16.821 0-9.462-7.885-16.821-17.347-16.821h-136.15c-9.462 0-17.347 7.3593-17.347 16.821 0 9.462 7.885 16.821 17.347 16.821h50.99v410.02h-50.99c-9.462 0-17.347 7.3594-17.347 16.821 0 9.462 7.885 17.347 17.347 17.347zm323.29-102.51c19.45 0 35.22-15.77 35.22-35.22v-202.91c0-18.398-14.193-33.643-32.591-34.694l-2.6283-0.52567h-305.41c-18.924 0-34.694 15.77-34.694 35.22v202.91c0 19.45 15.77 35.22 34.694 35.22z"/>' .
        '</svg>';
    }

    return $html;
  }

  protected function getTitle(): string
  {
    return 'n3t Fields';
  }

  protected function getPanelBody(): string
  {
    $html = '<table class="tracy-sortable"><tbody>';

    foreach($this->data as $name) {
      $html.= '<tr>';
      $html.= '<td>'.\Tracy\Dumper::toHtml($name, array(\Tracy\Dumper::COLLAPSE => true)).'</td>';
      $html.= '<tr>';
    }

    $html.= '</tbody></table>';

    return $html;
  }

}
