<?php
/**
 * @package n3t Fields
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2017-2024 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined( '_JEXEC' ) or die( 'Restricted access' );

use Joomla\Registry\Registry;

class n3tFieldsXml {

  public static function fieldsToXml($fields)
  {
    $xml = '<?xml version="1.0" encoding="utf-8"?'.'>' . "\n";
    $xml.= '<form>' . "\n";
    $xml.= '	<fields name="n3tfields">' . "\n";
    foreach ($fields as $group) {
      $xml.= '		<fieldset name="' . $group['name'] . '" label="' . $group['label'] . '"';
      if (isset($group['description']) && !empty($group['description']))
        $xml.= ' description="' . $group['description'] . '"';
      $xml.= '>' . "\n";
      foreach ($group['fields'] as $field) {
        $xml.= '			<field';
        foreach ($field as $name => $value) {
          if ($name == 'options') continue;
          if ($name == 'required') {
            if ($value)
              $xml.= ' ' . $name;
          } elseif ($value)
            $xml.= ' ' . $name . '="' . $value . '"';
        }
        if (isset($field['options']) && is_array($field['options'])) {
          $xml.= '>' . "\n";
          foreach($field['options'] as $value => $label)
            $xml.= '			  <option value="' . $value . '">' . $label . '</option>' . "\n";
          $xml.= '			</field>' . "\n";
        } else
          $xml.= ' />' . "\n";
      }
      $xml.= '		</fieldset>' . "\n";
    }
    $xml.= '	</fields>' . "\n";
    $xml.= '</form>' . "\n";

    return $xml;
  }

}
