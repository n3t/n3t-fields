<?php
/**
 * @package n3t Fields
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2017-2024 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined( '_JEXEC' ) or die( 'Restricted access' );

use Joomla\CMS\Cache\Cache;
use Joomla\CMS\Factory;
use Joomla\Registry\Registry;

class n3tFields {

  const FORMAT_RAW = 'raw';
  const FORMAT_JSON = 'json';
  const FORMAT_JSON_ARRAY = 'json_array';

  protected static $contexts = array();

  public static function registerContext($name, $context)
  {
    self::$contexts[$name] = $context;
  }

  public static function __callStatic($name, $arguments)
  {
    if (isset(self::$contexts[$name])) {
      $arguments = array_merge(array(self::$contexts[$name]), $arguments);
      return call_user_func_array(array(__CLASS__, '_'), $arguments);
    }

    throw new Exception('Context ' . $name . ' is not supported.');
  }

  protected static function cacheId($context, $item)
  {
    return md5($context . '.' . $item);
  }

  public static function getValues($context, $item)
  {
    static $cache = array();

    if(!isset($cache[self::cacheId($context, $item)])) {
      $db = Factory::getDbo();
      $query = $db->getQuery(true)
        ->select($db->quoteName('value'))
        ->from('#__n3tfields')
        ->where($db->quoteName('context') . ' = ' . $db->quote($context))
        ->where($db->quoteName('item_id') . ' = ' . $item);
      $db->setQuery($query);

      $values = new Registry();
      $values->loadString($db->loadResult() ?? '');

      $cache[self::cacheId($context, $item)] = $values;
    }

    return $cache[self::cacheId($context, $item)];
  }

  public static function cleanCache($context, $item)
  {
    if (is_object($item) && isset($item->id))
      $item = $item->id;

    if (empty($context) || empty($item))
      return null;

		$conf = Factory::getConfig();

		$options = array(
      'defaultgroup' => 'n3tfields',
      'storage'      => $conf->get('cache_handler', ''),
      'caching'      => true,
			'cachebase'    => $conf->get('cache_path', JPATH_SITE . '/cache')
		);

    $cache = Cache::getInstance('callback', $options);

    $cache->remove(self::cacheId($context, $item));
  }

  public static function _($context, $item, $name = null, $default = null, $format = self::FORMAT_RAW)
	{
    if (is_object($item) && isset($item->id))
      $item = $item->id;

    $item = (int)$item;

    if (empty($context) || empty($item))
      return null;

    $cache = Factory::getCache('n3tfields');

    $values = $cache->get(array(__CLASS__, 'getValues'), array($context, $item), self::cacheId($context, $item));

    $value = $name ? $values->get($name, $default) : $values;

    if ($name && $value && is_string($value)) {
      if ($format == self::FORMAT_JSON)
        $value = json_decode($value);
      elseif ($format == self::FORMAT_JSON_ARRAY)
        $value = json_decode($value, true);
    }

    return $value;
	}
}
