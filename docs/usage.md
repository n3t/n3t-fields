Usage
=====

Installation
------------

n3t Fields could be installed as any other extension in Joomla! For detailed
information see Joomla documentation.

Do not forget to __enable the plugin__ after installation. 

Joomla! versions
----------------

Currently only Joomla 3.x and newer is supported.

More information could be found in [Release notes](about/release-notes.md)

Extensions support
------------------

n3t Fields uses standard Joomla! way for forms modification. In other words,
it is supported by all extensions that uses this standard form implementation.

Unfortunately, until now there are a lot of components and Joomla! extensions, that 
rather use their own form definition style, and do not support standard Joomla
Form API.

With such extensions n3t Fields will not work, and there is nothing we could do
about that. If you want to use n3t Fields with such extension, please contact its
developer and ask him to enable standard Joomla! forms API in his/her extension.
