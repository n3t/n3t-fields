n3t Fields
==========

![Version](https://img.shields.io/badge/Latest%20version-5.0.0-informational)
![Release date](https://img.shields.io/badge/Release%20date-2024--02--08-informational)

n3t Fields adds ability to define extra fields for various Joomla administration
parts directly in template.

You can easily specify custom fields directly in your template folder. Fields could
be defined for

- Articles
- Categories
- Modules
- Menu items
- Banners
- Users
- and many more...
