FieldsAttach Conversion
=======================

[FieldsAttach][FieldsAttach] is Joomla extension developed by Cristian Percha.
It allows to define fields for articles and categories, and it was here long time
before Joomla came with its native solution. Currently FieldsAttach are not actively
developed anymore, if you are still stucked with it, you can easily convert its
values to n3t Fields, and modify your templates to use n3t Fields.

Conversion
----------

Open n3t Fields plugin configuration and go to __FieldsAttach convertor__ page.
If FieldsAttach is installed on your site, and it contains published fields, you
should see list of those fields here, together with information if it is field for
categories, or for articles, name of its group, proposed conversion type of field
and context and finally input, where you can enter field name you woul like to use
in n3t Fields.

Note that only basic FieldsAttach fields types are supported at the moment, all
others will be converted as simple text fields. Also note, that gallery field type is
converted to format used by n3t Image Gallery plugin.

When choosing field name, keep in mind, that the name will be used in XML definition,
so you should follow standards in field naming in Joomla, use only lowercase letters,
numbers and underscores.

If you save plugin configuration, your prefilled field names will stay on its place,
so you can prepare it without worrying to loose it.

When you are ready to go, just click __Convert__ button under the list of fields,
and wait for results. n3t Fileds will convert all defined fields, and propose you XML
files for every converted context, plus language files for your template with fields
titles.

__Preview XML__ file will display same results as conversion itself, but it does not
convert any value. It could be usefull when preparing the conversion.

Overwrite values
----------------

Above the list of fields you have option to select whether to overwrite existing
n3t Fields values or not. When converting fields on field name, that already exists
in n3t Fields, by default only values that are not converted or manually defined
yet will be converted. If overwrite option is selected, all values will be overwritten.

Important
---------

After conversion, before checking the article fields values, __do not forget__ to
[save proposed XML files to your template](../fields/definition.md) or create your own
definitions, otherwise no field will be displayed, and when saving the article you
will loose you converted values.

Note
----

In comparison to FieldsAttach, there is no automatic display of n3t Fileds values,
you have to [handle this by yourself](../fields/usage.md) in your template files.

[FieldsAttach]: https://github.com/Percha/fieldsattach_j3