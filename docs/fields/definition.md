Fields definition
=================

Custom fields could be defined easily by standard Joomla! XML form definition file.
To define fields for specific component, first you need to find out its context,
than create definition files, and store it within your template folder struxture.

Finally modify your template to use such fields in any way you want.

Context
-------

First of all you need to find context of the specific form, you would like to extend
by custom fields. In most cases this is name of component, followed by name of view.

Example could be `com_content.article`, `com_modules.module`, `com_menus.item` etc.

Some extensions could use different contexts, if so you have to find the context from the
code of the extension, or ask the developer of such extension.

For com_categories, there is exclusion for this rule. To make definition for categroies
specific for every extension, the context is always `com_extension.category`, so
for example `com_content.category`.

Files structure
---------------

If you know the context, you are ready to create your fields definition file.

In your template folder create subfolder named `n3tfields`. Now create subfolder
with name before first dot in context. If context contains more than 1 dot, create
as many subfolders, as are contained in context.

The last part of context, after the last dot, specifies name of xml file, containing
fields definition.

So for example, for context `com_content.article`, you should create file named
`article.xml` and place it to folder `n3tfields/com_content` within your template folder.

Example of complex file structure could look like this:
```
..
n3tfields
  com_content
    article.xml
    category.xml
  com_modules
    module.xml
  com_users
    user.xml
```

Some contexts supports contexs extensions. For example `com_modules.module` context
supports specific files per module type. In such case form module `mod_custom` could
file structure look like this:
```
..
n3tfields
  com_modules
    module
      mod_custom.xml
    module.xml
```

In this case all fields defined in `module.xml` will be available for all modules,
and fields in `module/mod_custom.xml` will be available just for mod_custom.

Note that fields defined in `module/mod_custom.xml` still have context `com_modules.module`,
so you can access it same way as any other field defined directly in `module.xml`,
they have just separated definition.

Files content
-------------

XML definition files has to be standard Joomla! [XML form definiftion files][JFORM].
Take a look in Joomla! documentation to find more. Also list of [Standard Joomla! fields][JFIELDS]
could be useful.

To make things easier, n3t Fields also integrates Joomla! fields plugins, so if you install
custom fields plugin, you could use it directly in your file, without need to add
`addfieldspath` syntax.

Fields itself has to be named `n3tfields`. Fieldsets could be generally named however you want,
but not all components (com_banners for example) supports all fieldsets. In such case,
you have to use some fieldset name supported by component view.

Example of simple fields definition for articles could be:
```
<?xml version="1.0" encoding="utf-8"?>
<form>
	<fields name="n3tfields">
		<fieldset name="myparams" label="My extra fields">
      <field name="my_field" type="text" label="My field" />
		</fieldset>
	</fields>
</form>
```

If you save this file as `n3tfields\com_content\article.xml`, your article definition
page will be extended by tab named 'My extra fields', with one text field named
'My field'.

Note that for labels, descriptions or any other translatable content in definition files
you could use language constants placed directly in your template language file.
These should be place in `language` subfolder of your template and are loaded automatically
by n3t Fields.

Multiple templates
------------------

Fields definition is shared between templates. In administration, all templates are
being serached for fields definition, ordered alphabetically by template name.
So, if two fields with same name are defined in two different templates, later
definition will be used (that in template, whis is alphabetically ordered as later).

This could be usefull, if you plan to share fields values between templates (for
example Article subtitle). There is small pitfall, if you are not carefull with name of fields,
it could happen, that different field type is loaded than you expected.

[JFORM]: https://docs.joomla.org/XML_JForm_form_definitions
[JFIELDS]: https://docs.joomla.org/Standard_form_field_types
