Context extensions
==================

Following list contains list of contexts supporting context extensions.

Modules
-------

Core Joomla! modules component.

Supported context extensions are:

####`com_modules.module`
_module type_ - fields for module of specific type.

File for such fields will be, for example, `com_modules/module/mod_custom.xml`
for Joomla! core Custom content module.

Menu items
----------

Core Joomla! menu component.

Supported context extensions are:

####`com_menu.item`
_option_ - fields for menu item of specific component.

File for such fields will be, for example, `com_menu/item/com_wrapper.xml`
for menu item pointing Joomla! core Wrapper component.

Custom fields
-------------

Core Joomla! custom fields component.

Supported context extensions are:

####`com_fields.field`
_context_ - fields for custom fields of specific component context.

File for such fields will be, for example, `com_menu/field/com_content/article.xml`
for custom fields for content articles.
