Known contexts
==============

Following list contains known components, that are able to work with n3t Fields,
and tested contexts to use with it. This is not complete list, it is just list of
__tested__, fully functional contexts.

Articles
--------

Core Joomla! content component.

Supported contexts are:

- `com_content.article` - fields for article
- `com_content.category` - fields for articles category

Supported contexts shortcodes are:

- `article` for `com_content.article`
- `category` for `com_content.category`

Banners
-------

Core Joomla! banners component.

Supported contexts are:

- `com_banners.banner` - fields for banner. Note that only supported fieldset are
  `otherparams` (displayed on Banner details tab), `image` (displayed on details tab
  above Click URL field, when banner is image type) and `metadata` (displayed on
  Publishing tab in the right column). This limitation is caused by Joomla administrative
  template not by n3t Fields plugin.
- `com_banners.category` - fields for banners category

Supported contexts shortcodes are:

- `banner` for `com_banners.banner`

Contacts
--------

Core Joomla! contacts component.

Supported contexts are:

- `com_contact.contact` - fields for contact
- `com_contact.category` - fields for contacts category

Supported contexts shortcodes are:

- `contact` for `com_contact.contact`

Custom fields
-------------

Core Joomla! custom fields component.

Supported contexts are:

- `com_fields.field` - fields for custom fields
- `com_fields.field.com_content.article` - fields for custom fields for content articles 
  (similar way any other supported component)
  
Supported contexts shortcodes are:

- `field` for `com_fields.field`

Menus
-----

Core Joomla! menus component.

Supported contexts are:

- `com_menus.item` - fields for menu item

Supported contexts shortcodes are:

- `menu` for `com_menus.item`

Modules
-------

Core Joomla! modules component.

Supported contexts are:

- `com_modules.module` - fields for module

Supported contexts shortcodes are:

- `module` for `com_modules.module`

Newsfeeds
---------

Core Joomla! newsfeeds component.

Supported contexts are:

- `com_newsfeeds.newsfeed` - fields for newsfeed
- `com_newsfeeds.category` - fields for newsfeeds category

Tags
----

Core Joomla! tagss component.

Supported contexts are:

- `com_tags.tag` - fields for tag

Supported contexts shortcodes are:

- `tag` for `com_tags.tag`

Users
-----

Core Joomla! users managment component.

Supported contexts are:

- `com_users.user` - fields for user profile
- `com_users.category` - fields for user notes category

Supported contexts shortcodes are:

- `users` for `com_users.user`

Weblinks
--------

Core Joomla! weblinks component.

Supported contexts are:

- `com_weblinks.weblink` - fields for weblink
- `com_weblinks.category` - fields for weblinks category
