Usage
=====

n3tFields Class
---------------

To make usage of custom fields as simple as possible, n3t Fields introduce class
n3tFields. Usage is very simple:

```
<?php
$value = n3tFields::_('com_content.article', $item, 'myfield', 'Default value');
?>
```

You can see that the class provides 1 static method, named `_`, with five parameters:

- `$context` - context of the field you would like to retrieve.
- `$item` - this could be either id of the item (article, module etc..), or an
  object containing id property.
- `$name` - name of the field, you would like to retrieve. This has to correspond
  with name attribute of the field in definition file.
- `$default` - default return value, if the field is not found, or if the field
  value is empty. This parameter could be ommited.
- `$format` - format of the field. Supported are raw values and JSON encoded values.
  Raw format is default, and it returns field value as is. JSON format will consider
  field value as JSON encoded and will try to decode it. This parameter is considered
  only if `$name` parameter is specified. See chapter JSON encoded fields in this
  documentation for more info.

You do not need to call any `require` or `include`, as the class is automatically
registered by plugin itself.

To make the code clean, and to be sure, that n3t Fields plugin is installed and enabled,
you could use code similar to this:

```
<?php
if (class_exists('n3tFields'))
  $value = n3tFields::_('com_content.article', $item, 'myfield', 'Default value');
else
  $value = 'Default value';
?>
```

#### Retrieving all fields

If you ommit `$name` parameter (or pass null), returned value will be [Registry object][JREGISTRY]
containing all fields for selected context and item.

```
<?php
$values = n3tFields::_('com_content.article', $item);
?>
```

#### Shortcodes

n3tFields class provides also method `registerContext`, which allows to create
shortcode functions for specific context. It has two parameters:

- `$name` - name of the newly created function.
- `$context` - shortcode context.

For example, to create shortcode for article values, you could do it like this:
```
<?php
n3tFields::registerContext('article', 'com_content.article');
$value = n3tFields::article($item, 'myfield', 'Default value');
?>
```

#### Default shortcodes

n3t Fields comes with few predefined shortcodes:

- `article` for `com_content.article`
- `banner` for `com_banners.banner`
- `category` for `com_content.category`
- `contact` for `com_contacts.contact`
- `field` for `com_fields.field`
- `menu` for `com_menus.item`
- `module` for `com_modules.module`
- `tag` for `com_tags.tag`
- `user` for `com_users.user`

#### JSON encoded fields

When working with JSON encoded fileds (such as galleries, multiple values etc.),
you can retrieve field value normal way, and then decode it, or you can use `$format`
parameter, which will do the work for you:

```
<?php
$value = n3tFields::article($item, 'gallery', null, n3tFields::FORMAT_JSON);
?>
```

Caching
-------

n3tFields class automatically stores results of function `_` in memory, so multiple
calls for same item within one page rendering will not trigger multiple SQL queries.
Following code will trigger just 1 SQL query:
```
<?php
$value = n3tFields::article($item, 'myfield', 'Default value');
$value2 = n3tFields::article($item, 'myfield2');
?>
```

More over, if cache is enabled in global Joomla! configuration, function results
are stored within Joomla! cache, so even multiple page rendering will trigger just
one SQL query for the first render.

Cache for fields is automatically cleared upon saving the item.

[JREGISTRY]: https://api.joomla.org/cms-3/classes/Joomla.Registry.Registry.html
