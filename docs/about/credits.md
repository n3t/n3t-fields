Credits
=======

Thanks to these projects and contributors:

- [Joomla! CMS][Joomla] withou whic, obvously, this extension will not exist
- [Bongovo!][BONGOVO] for Czech and Slovak translation
- [Czech Joomla community][JOOMLAPORTAL] for testing, support and comments on development

[JOOMLA]: https://www.joomla.org
[BONGOVO]: https://www.bongovo.cz/
[JOOMLAPORTAL]: https://www.joomlaportal.cz/