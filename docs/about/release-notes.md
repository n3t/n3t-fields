Release notes
=============

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

[Unreleased]
------------

- no unreleased changes

[5.0.x]
-------

### [5.0.0] - 2024-02-08

#### Changed
- unknown context throws an exception now
#### Fixed
- PHP 8 warnings
#### Added
- Joomla 5 compatibility

[4.0.x]
-------

### [4.1.0] - 2023-09-11

#### Changed
- improved com_fields support
- minimal PHP requirement is 7.4 now
- improved installer script
#### Removed
- FieldsAttach convertor

### [4.0.6] - 2022-07-12

#### Fixed
- SQL errors on updating to 4.0.5

### [4.0.5] - 2022-06-30

#### Changed
- improved update schema to show changes in Joomla Database check

### [4.0.4] - 2022-06-10 

#### Added
- n3t Debug support (shows current available contexts)
#### Changed
- FieldsAttach component detection changed to database tables detection

### [4.0.3] - 2020-07-20

#### Fixed
- solved context `com_contact.contact` administration form with frontend contact form conflict

### [4.0.2] - 2020-01-23

#### Fixed
- solved invalid context `contact`

### [4.0.1] - 2019-08-26

#### Added
- n3tFields helper supports FORMAT_JSON_ARRAY to decode JSON encoded values as array instead of object
#### Changed
- Improved installer

### [4.0.0] - 2019-04-19

#### Added
- Joomla! 4 compatibility
- extended menu items context

[3.1.x]
-------

### [3.1.1] - 2018-09-07

#### Added
- n3tFields helper class support for JSON encoded values
- extended modules context

### [3.1.0] - 2018-04-20

#### Added
- `contact` shortcode added
- `tag` shortcode added
- FieldsAttach convertor added
#### Fixed
- solved warning displayed in some unsupported extensions

[3.0.x]
-------

### [3.0.1] - 2017-10-06

#### Changed
- Renamed to make JED happy

### [3.0.0] - 2017-09-18

- Initial release
